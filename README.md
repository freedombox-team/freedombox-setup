# FreedomBox Setup

This module is **OBSOLETE**. It is simply a Debian package that depends on
`freedombox` package. Instead of this module, install and use [FreedomBox
Service (Plinth)](https://salsa.debian.org/freedombox-team/plinth/) provided on
Debian machines by the package `freedombox`. This module used to perform some
basic system-wide setup operations for FreedomBox. All of its functionality has
since been moved to FreedomBox Service (Plinth).
